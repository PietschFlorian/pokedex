export const getImage = (id = 1) => {
    const image = require(`pokemon-sprites/sprites/pokemon/other/official-artwork/${id}.png`).default
    return image
}