import React from "react"
import { getImage } from "../../util"
import { Card, Paper, Grid } from "@mui/material"

const Thumbnail = ({ data, page }) => {
  console.log(data, page)


  return (
    <Grid container spacing={2}>
      {Object.values(data.results).map((pokemon, idx) => (
        <Grid item xs={12} sm={6} md={4} lg={3} xl={3}>
          <Paper elevation={3} style={{textAlign:'center'}}>
            <p>{pokemon.name}</p>
            <img src={getImage(helper(idx, page) + 1)} />
          </Paper>
        </Grid>
      ))}
    </Grid>
  )
}

export default Thumbnail

const helper = (idx, page) => {
  if(page === 0){
    return idx
  } else {
    return(idx)+(20*page)
  }
}

/* 
TODO back to top button auf absolute
TODO next und previous button oben rechts
TODO in der Mitte suchleiste bei man mit id und name suchen kann
(TODO da so suchmaschinenzeug einbauen?)

*/
