import React from "react"

const Name = (data, page) => {

    return(
        <table>
        {Object.values(data.results).map((pokemon, idx) => (
          <tr>
            <td>
              <p>
                {idx + 1 + page} {pokemon.name}
              </p>
            </td>
          </tr>
        ))}
      </table>
    )
}

export default Name