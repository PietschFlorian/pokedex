import React, { useEffect, useState } from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import axios from "axios"
import test from "pokemon-sprites/sprites/pokemon/other/official-artwork/1.png"
import { getImage } from "../components/util"
import { Button, Card, Paper, Stack, Grid, Typography } from "@mui/material"
import Name from "../components/pokemon/layouts/name"
import Thumbnail from "../components/pokemon/layouts/thumbnail"



const Pokemon = () => {
  const [data, setData] = useState(null)
  const [page, setPage] = useState(0)
  const [displayMode, setDisplayMode] = useState("THUMBNAIL")
  console.log(data)

  useEffect(() => {
    const fetchData = async () => {
      axios
        .get(`https://pokeapi.co/api/v2/pokemon?offset=${20 * page}&limit=20`)
        .then(response => setData(response.data))
    }
    fetchData()
  }, [page])

  const handleNext = () => {
    setPage(prev => prev + 1)
  }

  const handlePrevious = () => {
    setPage(prev => prev - 1)
  }

  if (!data) return <p>loading</p>

  return (
    <>
    {/* links toggle fuer displaymode und mitte suchleiste */}
    {/* page anzeige zu dropdown und seite auswaehlbar machen */}
      <Grid container spacing={2} direction={'row'} justifyContent={'space-between'}>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            disabled={page === 65}
            onClick={handleNext}
          >
            NEXT
          </Button>
        </Grid>
        <Grid item>
          <Typography variant="body1">{page}/65</Typography>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            disabled={page === 0}
            onClick={handlePrevious}
          >
            PREVIOUS
          </Button>
        </Grid>
      </Grid>
      {displayMode === "NAME" && <Name data={data} page={page} />}
      {displayMode === "THUMBNAIL" && <Thumbnail data={data} page={page} />}
    </>
  )
}

export default Pokemon
